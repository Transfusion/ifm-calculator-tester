import React, { useEffect, useState } from "react";
import styled from "styled-components";

import Tree from "react-d3-tree";
import { VISUALIZE_TREES_URL } from "./constants";
// import logo from './logo.svg';
// import './App.css';

const defaultPersonalProdValues = {
  HA: [56820],
  XA1: [39480],
  XA2: [14580],
  XA3: [3500],
  XB12: [7200],
  XB1: [53810],
  XB2: [2830],
  XA5: [12010],
  XB9: [4150],
  XA7: [5000],
  XB10: [1000],
  XB11: [2500],

  XB3: [3010],
  XB6: [9810],
  XB7: [1940],
  XA6: [15000],
  XB8: [8780],
  XB13: [3980],

  XB4: [9900],
  XB5: [10300],
};

type VizTree = {
  name: string;
};

const ProdValuesTextArea = styled.textarea`
  padding: 10px;
  margin: 10px;
  width: 80%;
  min-height: 500px;
`;

function App() {
  const [fullTrees, setFullTrees] = useState([]);
  const [prodValues, setProdValues] = useState(
    JSON.stringify(defaultPersonalProdValues, null, 2)
  );

  const fetchTrees = async () => {
    const trees = await (await fetch(VISUALIZE_TREES_URL)).json();
    setFullTrees(trees);
  };

  useEffect(() => {
    fetchTrees();
  }, []);

  return (
    <div className="App">
      {fullTrees.map((tree: VizTree, idx) => {
        return (
          <>
            <div style={{ padding: "10px" }}>
              Tree #{idx + 1}, root {tree.name}
            </div>
            <div
              id={`treeWrapper-${idx}`}
              style={{
                width: "100%",
                height: "550px",
                borderBottom: "1px solid grey",
              }}
            >
              <Tree data={tree} orientation="vertical" />
            </div>
          </>
        );
      })}

      <ProdValuesTextArea value={prodValues} />

      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    </div>
  );
}

export default App;
